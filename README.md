# MeFitFrontend

This is the frontend application for the MeFit application.
MeFit is an application for people who like to accelerate their fitness process. In 
this application the user will be able to find regular and customized workouts. The 
user can set weekly goals of workouts, exercise, programs and see if they have 
skipped anything. The user can also apply to be a contributor, they can create their 
own workout sessions, this workout can even include self-made videos explaining 
each exercise. MeFit gives the users the opportunity to find good quality content, 
the business logic makes sure users will contribute with different exercises, which 
results with self-growing content. We always had kept expendability in mind. So 
the thing that would make this app successful is creating an atmosphere where a 
community easily shares, cares and grows.

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)


## Table of Contents
- [Background](#background)
- [Install](#install)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

### Project Approach
The goal of this project was to create a fitness app with teammates. The MeFit 
application is built by 4 junior developers and our purpose was to build it the most 
effective way. We decided to use Scrum, which is a pre-defined development 
lifecycle based on agile principles. Agile methodologies promote a project-
management process that encourages frequent inspection and adaptation, and a 
leadership philosophy using teamwork, self-organization and accountability. 

### Task Distribution
Two of us were responsible for writing the user stories. Thereafter the stories were
discussed by the whole team. We decided which future was a must, could or 
should. All these information about the tasks and priorities were stored in an app 
called Trello. The first task of the team was to create an ERD (Entity Relationship 
Diagram) of the potential database and create models for the login, registration, 
dashboard, and profile page. We measured how much time each feature would 
take, what the most efficient way to handle a certain task is.

### Architecture
This application is built by 3 different technologies. We used Angular for the 
front-end, Spring Boot for the back-end and PostgreSQL for the database, Auth0 
for the security.
The design of our database is visualized in an [ERD](src/assets/images/ERD.svg).
Respective classes are structured according the [class diagram](src/assets/images/Class%20Diagram.png).

### Technology Stack
The MeFit application consists of three parts: (i) frontend, (ii) backend, and (iii) authentication.
#### Frontend:
The frontend is deployed on [Heroku](https://make-mefit.herokuapp.com/), the code is stored in this repository.
* Angular version 13.3.3
* Angular Material

#### Backend:
The API is deployed to [Heroku](https://mefit-api.herokuapp.com/swagger-ui/index.html), the code is stored in a [separate repository](https://gitlab.com/ceesparis/mefit_backend).
* Java OpenJDK version 17.0.2 with ​
* Gradle build system​
* Spring (boot, data, web)​
* Mapstruct​
* OpenAPI (formerly called Swagger)
* PostgreSQL​ dialect

#### Authentication (Login Functionality):
Login functionality was obtained with a self-hosted [Keycloak](https://mefit-security.herokuapp.com/auth/) instance.

### Install
Most conveniently you can run the app via https://make-mefit.herokuapp.com/.
If you want to run the application locally, start the backend server before the frontend client.
#### Backend:
1. Install PostgreSQL
2. Configure data source in application.yml
3. cd backend
4. Run gradle install
5. Run gradle spring-boot:run
6. Spring Boot will import mock data into database by
7. The backend server is running on localhost:8080
#### Frontend:
1. Install Node.js and npm
2. cd frontend
3. Run npm install
4. Change resource in the [keycloak.json](src/assets/keycloak.json) file to "mefit"
5. Make sure the backendAPI in [environment.ts](src/environments/environment.ts) is set to "http://localhost:8080/api/v1/".
4. Run ng serve
5. The frontend client is running on localhost:4200.

## Maintainers

* [@ceesparis](https://gitlab.com/ceesparis)
* [@hilmi46](https://gitlab.com/hilmi46)
* [@hubermarkus](https://gitlab.com/hubermarkus)
* [@SophiaKunze](https://gitlab.com/SophiaKunze)

## Contributing

This projects follows the [Contributor Covenant](http://contributor-covenant.org/version/1/3/0/) Code of Conduct.
Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

### Acknowledgement
This project exists thanks to <a href=https://www.experis.de/de>Experis</a> and our teachers <a href="https://gitlab.com/NicholasLennox">Nicholas Lennox</a> and <a href="https://gitlab.com/sumodevelopment">Dewald Els</a>.

## License
[MIT](https://opensource.org/licenses/MIT) © 2022 Markus Huber, Hilmi Terzi, Cees Paris, and Sophia Kunze