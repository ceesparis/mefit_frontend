export const environment = {
  production: true,
  apiKey: "a209b89a-1624-4b8e-8ecc-b13bdeae0702",
  backendAPI: "https://mefit-api.herokuapp.com/api/v1/",
  profilesEndpoint: "profiles",
  goalEndpoint: "goals",
  exerciseEndpoint: "exercises",
  workoutsEndpoint: "workouts",
  programsEndpoint: "programs",
  usersEndpoint: "users",
  profileEndpoint: "profile",
};
