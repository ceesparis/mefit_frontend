// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // backendAPI: "https://mefit-api.herokuapp.com/api/v1/",
  apiKey: "a209b89a-1624-4b8e-8ecc-b13bdeae0702",
  backendAPI: "http://localhost:8080/api/v1/",
  profilesEndpoint: "profiles",
  goalEndpoint: "goals",
  exerciseEndpoint: "exercises",
  workoutsEndpoint: "workouts",
  programsEndpoint: "programs",
  usersEndpoint: "users",
  profileEndpoint: "profile",
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
