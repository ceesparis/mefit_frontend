import { Role } from "../enums/role";
import { Profile } from "./profile.model";

export interface User{
    id: string;
    profileId: number;
    username: string;
    roles: Role[];
}