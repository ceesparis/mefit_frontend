import { WorkoutExercise } from "./workout-exercise.model";
import { Workout } from "./workout.model";

export interface Goal {
    id: number;
    profileId: number;
    workouts: Workout[];
    pendingWorkouts: Workout[];
    completedWorkouts: Workout[];
    startDate: string;
}