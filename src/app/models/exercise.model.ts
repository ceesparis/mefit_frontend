import { MuscleGroup } from '../enums/muscle-group';

export interface Exercise {
  id: number;
  lastEditorId: number;
  creatorId: number;
  name: string;
  targetMuscleGroups: MuscleGroup[];
  description: string;
  exerciseId?: number;
  imageURL?: string;
}

// for POST requests
export interface NewExercise {
  name: string;
  targetMuscleGroups: MuscleGroup[];
  description: string;
  imageUrl: string;
  videoUrl: string;
  creatorId: number;
  lastEditorId: number;
}

// for GETTING an individual exercise
export interface ExerciseResponse extends NewExercise {
  id: number;
}
