import { Exercise } from "./exercise.model";

export interface WorkoutExercise extends Exercise{
    repetitions: number;
    sets: number;
}