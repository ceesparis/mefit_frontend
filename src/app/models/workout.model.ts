import { WorkoutType } from "../enums/workout-type";
import { WorkoutExercise } from "./workout-exercise.model";

export interface Workout {
    id: number;
    lastEditorId?: number;
    creatorId: number;
    name: string;
    type: WorkoutType;
    exercises: WorkoutExercise[];
    description?: string;
}