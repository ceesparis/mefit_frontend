import { ProgramCategory } from "../enums/program-category";
import { Workout } from "./workout.model";

export interface Program {
    id: number;
    lastEditorId: number;
    creatorId: number;
    name: string;
    category: ProgramCategory;
    workouts: Workout[]; 
    description: string
}

export interface NewProgram {
    name: string,
    category: ProgramCategory,
    description: string,
    lastEditorId: number,
    creatorId: number
}