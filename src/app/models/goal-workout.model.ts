import { Workout } from "./workout.model";

export interface GoalWorkout extends Workout{
    workoutId?: number;
    completed?: boolean;
    done?: boolean;
}