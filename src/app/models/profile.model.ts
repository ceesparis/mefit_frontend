import { FitnessLevel } from '../enums/fitness-level';

export interface Profile {
  id: number;
  firstName: String;
  lastName: String;
  pictureUrl: String;
  weightKg: number;
  heightCm: number;
  birthday: Date;
  fitnessLevel: FitnessLevel;
}

export interface ProfileResponse {
  id: number;
  firstName: string;
  lastName: string;
  pictureUrl: string;
  weightKg: number;
  heightCm: number;
  birthday: Date;
  fitnessLevel: FitnessLevel;
  editedWorkouts: number[];
  editedPrograms: number[];
  editedExercises: number[];
  createdWorkouts: number[];
  createdPrograms: number[];
  createdExercises: number[];
  goals: number[];
}
