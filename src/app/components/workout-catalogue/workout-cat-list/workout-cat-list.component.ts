import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { MatGridTileHeaderCssMatStyler } from '@angular/material/grid-list';
import { filter } from 'rxjs';
import { WorkoutType } from 'src/app/enums/workout-type';
import { Exercise } from 'src/app/models/exercise.model';
import { WorkoutExercise } from 'src/app/models/workout-exercise.model';
import { Workout } from 'src/app/models/workout.model';
import { ExerciseService } from 'src/app/services/exercise.service';
import { WorkoutService } from 'src/app/services/workout.service';

@Component({
  selector: 'app-workout-cat-list',
  templateUrl: './workout-cat-list.component.html',
  styleUrls: ['./workout-cat-list.component.scss']
})
export class WorkoutCatListComponent implements OnChanges, OnInit {

  @Input() filters: WorkoutType[] = [];
  @Input() workouts: Workout[] = [];
  filteredWorkouts: Workout[] = this.workouts;


  constructor(private exerciseService: ExerciseService) { }

  // change filters according to changes
  ngOnChanges(changes: SimpleChanges) {
    this.filterWorkouts();
  }

  ngOnInit() {
    const exercises: Exercise[] = this.exerciseService.exercises;
    for (const workout of this.filteredWorkouts) {
      var actualExercises: WorkoutExercise[] = [];
      for (const exercise of workout.exercises) {
        if (exercise.exerciseId != undefined && exercises != undefined) {
          const actualExercise = exercises.filter(x => x.id == exercise.exerciseId).pop();
          if (actualExercise != undefined) {
            const actualWorkoutExercise: WorkoutExercise = {
              repetitions: exercise.repetitions, sets: exercise.sets, id: exercise.id,
              exerciseId: actualExercise.id, lastEditorId: actualExercise.lastEditorId,
              creatorId: actualExercise.creatorId, name: actualExercise.name,
              targetMuscleGroups: actualExercise.targetMuscleGroups,
              description: actualExercise.description, imageURL: actualExercise.imageURL
            }
            actualExercises.push(actualWorkoutExercise)
          }
        }
      }

      workout.exercises = actualExercises;
    }
  }

  // filter workouts according to current filters
  filterWorkouts() {
    if (this.filters.length > 0) this.filteredWorkouts = this.workouts.filter(x => this.filters.includes(x.type));
    else this.filteredWorkouts = this.workouts;
  }


}
