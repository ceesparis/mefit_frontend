import { Component, Input, OnInit } from '@angular/core';
import { Workout } from 'src/app/models/workout.model';

@Component({
  selector: 'app-workout-cat-list-item',
  templateUrl: './workout-cat-list-item.component.html',
  styleUrls: ['./workout-cat-list-item.component.scss']
})
export class WorkoutCatListItemComponent implements OnInit {

  @Input() workout?: Workout;
  constructor() { }

  ngOnInit(): void {
  }

}
