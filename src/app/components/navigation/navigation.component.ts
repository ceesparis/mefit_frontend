import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(public router: Router, private userService: UserService) { }

  ngOnInit(): void {
  }

  get authenticated(): boolean {
    return Boolean(keycloak.authenticated);
  }

  handleLogout(){
    keycloak.logout();
    // this.userService.logOutUser();
    // this.router.navigateByUrl("/login")
  }

}
