import { Component, Input, OnInit } from '@angular/core';
import { Exercise } from 'src/app/models/exercise.model';
import { WorkoutExercise } from 'src/app/models/workout-exercise.model';

@Component({
  selector: 'app-exercise-cat-list-item',
  templateUrl: './exercise-cat-list-item.component.html',
  styleUrls: ['./exercise-cat-list-item.component.scss']
})
export class ExerciseCatListItemComponent implements OnInit {

  // if I typecast as Exercise | WorkoutExercise,
  // it cannot find sets and repetitions because they are not attributes of exercise.
  // any typecast not the pretties, but ok for now
  @Input() exercise?: any;
  workoutExercise: boolean = false;
  
  constructor() { }

  ngOnInit(): void {
    if (this.exercise != undefined){
      if (this.exercise.hasOwnProperty("sets")) this.workoutExercise = true;
    }
  
  }

}
