import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MuscleGroup } from 'src/app/enums/muscle-group';
import { Exercise } from 'src/app/models/exercise.model';
import { WorkoutExercise } from 'src/app/models/workout-exercise.model';

@Component({
  selector: 'app-exercise-cat-list',
  templateUrl: './exercise-cat-list.component.html',
  styleUrls: ['./exercise-cat-list.component.scss']
})
export class ExerciseCatListComponent implements OnChanges {

  @Input() exercises: Exercise[] | WorkoutExercise[] = [];
  @Input() filters: MuscleGroup[] = [];
  filteredExercises: Exercise[] | WorkoutExercise[] = this.exercises;

  constructor() { }

  // whenever filter selection changes, change the exercises shown
  ngOnChanges(changes: SimpleChanges){
      this.filterExercises();
  }

  // filter exercises on current filter selection 
  filterExercises(){
    if (this.filters.length > 0) this.filteredExercises = this.exercises.filter(x => x.targetMuscleGroups.some(y => this.filters.includes(y)));
    else this.filteredExercises = this.exercises;
  }

}
