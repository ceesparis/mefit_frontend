import { Component, OnInit, Output, Input} from '@angular/core';
import { FormControl } from '@angular/forms';
import { EventEmitter } from '@angular/core';


@Component({
  selector: 'app-catalogue-filter',
  templateUrl: './catalogue-filter.component.html',
  styleUrls: ['./catalogue-filter.component.scss']
})
export class CatalogueFilterComponent implements OnInit {

  @Output() public selectedFilters= new EventEmitter<any[]>();
  @Input() selected: any[] = [];
  @Input() filterOptions: any[] = [];
  
  groups = new FormControl('');

  constructor() { }

  ngOnInit() {
  }

  // whenever the selected filters change, emit this change
  onFilterChange(){
    this.selectedFilters.emit(this.selected);
  }

}
