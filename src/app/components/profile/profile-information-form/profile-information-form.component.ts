import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { TooltipPosition } from '@angular/material/tooltip';
import { Profile } from 'src/app/models/profile.model';
import { ProfileService } from 'src/app/services/profile.service';

@Component({
  selector: 'app-profile-information-form',
  templateUrl: './profile-information-form.component.html',
  styleUrls: ['../profile-styles.scss'],
})
export class ProfileInformationFormComponent implements OnInit {
  
  @Input() profileId: number = 0
  @Input() profile?: Profile

  get profilePictureUrl(): string {
    const placeholderLink: string = "https://www.clipartmax.com/png/full/364-3643767_about-brent-kovacs-user-profile-placeholder.png"
    
    if(this.pictureUrl === null || this.pictureUrl.value === null){
      return placeholderLink
    }
    let content: string = this.pictureUrl.value
    if(content.length === 0){
      return placeholderLink
    }
    return this.pictureUrl?.value
  }

  constructor(
    private readonly fb: FormBuilder,
    private readonly profileService: ProfileService
  ) {}

  private _displaySuccess: boolean = false
  private _displayError: boolean = false


  get saving(): boolean {
    return this.profileService.saving
  }

  get displaySuccess(): boolean{
    return this._displaySuccess;
  }

  get displayError(): boolean{
    return this._displayError;
  }

  get error(): string{
    return this.profileService.error
  }

  ngOnInit(): void {
    this.profileForm.setValue({
      firstName: this.profile?.firstName,
      lastName: this.profile?.lastName,
      weight: this.profile?.weightKg,
      height: this.profile?.heightCm,
      birthday: this.profile?.birthday,
      fitnessLevel: this.profile?.fitnessLevel,
      pictureUrl: this.profile?.pictureUrl,
    });
  }

  // Using FormBuilder to create ProfileForm
  profileForm = this.fb.group({
    firstName: ['', Validators.required],
    lastName: [''],
    weight: [''],
    height: [''],
    birthday: [''],
    fitnessLevel: ['', Validators.required],
    pictureUrl: [''],
  });
  get firstName(){
    return this.profileForm.get('firstName')
  }
  get lastName(){
    return this.profileForm.get('lastName')
  }
  get weight(){
    return this.profileForm.get('weight')
  }
  get height(){
    return this.profileForm.get('height')
  }
  get birthday(){
    return this.profileForm.get('birthday')
  }
  get fitnessLevel(){
    return this.profileForm.get('fitnessLevel')
  }
  get pictureUrl(){
    return this.profileForm.get('pictureUrl')
  }
  
  submitProfileForm() {
    let profileToUpdate: Profile = {
      id: this.profileId,
      firstName: this.firstName?.value,
      lastName: this.lastName?.value,
      weightKg: this.weight?.value,
      heightCm: this.height?.value,
      birthday: this.birthday?.value,
      fitnessLevel: this.fitnessLevel?.value,
      pictureUrl: this.pictureUrl?.value, 
    };
    this.profileService.updateProfile(profileToUpdate)
    .subscribe({
      next: (profile: Profile) => {
        this.profileService.profile = profile;
        this.manageDisplaySuccess()
      },
      error: (error: HttpErrorResponse) => {
        this.profileService.error = error.message;
        this.manageDisplayError()
      },
    })
  }

  manageDisplaySuccess(): void {    
    this._displaySuccess = true;
    setTimeout(()=>{
      this._displaySuccess = false
    },3000)
  }

  manageDisplayError(): void {    
    this._displayError = true;
    setTimeout(()=>{
      this._displayError = false
    },6000)
  }

  // for tooltip
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];

}



