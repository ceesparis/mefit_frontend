import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-advanced-settings',
  templateUrl: './profile-advanced-settings.component.html',
  styleUrls: ['./profile-advanced-settings.component.scss']
})
export class ProfileAdvancedSettingsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  // Advanced Settings
  requestContributorStatus() {
    console.warn('Requested Contributor status.');
  }

  request2FA() {
    console.warn('Requested 2FA.');
  }

}
