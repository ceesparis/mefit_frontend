import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { TooltipPosition } from '@angular/material/tooltip';
import { Workout } from 'src/app/models/workout.model';
import { WorkoutService } from 'src/app/services/workout.service';

@Component({
  selector: 'app-workout-add',
  templateUrl: './workout-add.component.html',
  styleUrls: ['./workout-add.component.scss']
})
export class WorkoutAddComponent implements OnInit{

  // @Cees, I think it would be more apropriate to access the available workouts in here instead of passing them from the parent.
  // Do you agree? If yes - we can remove this input.
  //@Input() workouts: Workout[] = [] // allows passing data from a parent component on initialization of this component
  
  //@Input() selectedWorkout?: Workout // @Case - Are we using this Input at all?
  
  @Output() public selectedWorkoutEvent = new EventEmitter<Workout>();

  constructor(private fb: FormBuilder, private workoutService: WorkoutService) { }

  // workouts: Workout[] = this.workoutService.workouts

  addWorkoutForm = this.fb.group({
    workout: [undefined, [Validators.required]],
  });

  get workout(){return this.addWorkoutForm.get('workout')}

  addWorkout(){
    if (this.workout) this.selectedWorkoutEvent.emit(this.workout.value)
  }

  get workouts(): Workout[]{
    return this.workoutService.workouts;
  }

  ngOnInit(): void{
    this.workoutService.getAllWorkouts();
  }

  // for tooltip
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];

}
