import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { WorkoutService } from 'src/app/services/workout.service';
import { WorkoutExercise } from 'src/app/models/workout-exercise.model';
import { WorkoutType } from 'src/app/enums/workout-type';
import { TooltipPosition } from '@angular/material/tooltip';

@Component({
  selector: 'app-workout-edit',
  templateUrl: './workout-edit.component.html',
  styleUrls: ['./workout-edit.component.scss'],
})
export class WorkoutEditComponent {
  constructor(
    private fb: FormBuilder,
    private workoutService: WorkoutService
  ) {}

  // Dropdown-Values based on Enum
  workoutTypeValues = Object.keys(WorkoutType).filter((v) => isNaN(Number(v)));

  // Exercises that are to be added to this workout
  addedExercises: WorkoutExercise[] = [];

  // form for basic workout information
  workoutBasicsForm = this.fb.group({
    type: ['', [Validators.required]],
    name: ['', [Validators.required]],
    creatorId: [''],
  });

  // getters for form values
  get type() {
    return this.workoutBasicsForm.get('type');
  }
  get name() {
    return this.workoutBasicsForm.get('name');
  }
  get creatorId() {
    return this.workoutBasicsForm.get('creatorId');
  }


  // add WorkoutExercise to list when child component workout-add emits the respective event
  addWorkoutExercise(workoutExercise: WorkoutExercise){
    this.addedExercises.push(workoutExercise);
  }

  // update order of WorkoutExercises when User interacted with DragDropList
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(
      this.addedExercises,
      event.previousIndex,
      event.currentIndex
    );
  }

  // remove WorkoutExercise when User deleted it from DragDropList via Button-click
  removeExerciseFromWorkout(workoutExercise: WorkoutExercise) {
    this.addedExercises = this.addedExercises.filter(
      (we) => we !== workoutExercise
    );
  }

  saveWorkout() {
    // TODO call service with all necessary infromation to PUT/PATCH/POST this workout
    // 1. POST Workout

    // 2. PUT/Update Workout with respective foreign keys

  }

  // for tooltip
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
}
