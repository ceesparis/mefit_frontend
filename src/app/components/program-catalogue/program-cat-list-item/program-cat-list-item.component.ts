import { Component, Input, OnInit } from '@angular/core';
import { Program } from 'src/app/models/program.model';

@Component({
  selector: 'app-program-cat-list-item',
  templateUrl: './program-cat-list-item.component.html',
  styleUrls: ['./program-cat-list-item.component.scss']
})
export class ProgramCatListItemComponent implements OnInit {

  constructor() { }
  @Input() program?: Program;

  ngOnInit(): void {
  }

}
