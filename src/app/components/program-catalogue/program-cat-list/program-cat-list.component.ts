
import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Program } from 'src/app/models/program.model';

@Component({
  selector: 'app-program-cat-list',
  templateUrl: './program-cat-list.component.html',
  styleUrls: ['./program-cat-list.component.scss']
})
export class ProgramCatListComponent implements OnChanges {

  @Input() programs: Program[] = [];
  @Input() filters: any[] = [];
  filteredPrograms: any[] = this.programs;


  constructor() { }

  // if filter selection changes, filter programs accordingly
  ngOnChanges(changes: SimpleChanges){
      this.filterPrograms();
  }

  // filter programs according to current selection
  filterPrograms(){
    if (this.filters.length > 0) this.filteredPrograms = this.programs.filter(x => this.filters.includes(x.category));
    else this.filteredPrograms = this.programs;
  }

}
