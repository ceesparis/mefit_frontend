import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Program } from 'src/app/models/program.model';
import { Workout } from 'src/app/models/workout.model';

@Component({
  selector: 'app-program-selector',
  templateUrl: './program-selector.component.html',
  styleUrls: ['./program-selector.component.scss']
})
export class ProgramSelectorComponent implements OnInit {

  @Output() public addedWorkouts= new EventEmitter<Workout[]>();
  @Input() programs: Program[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  onAddToGoal(workoutsFromProgram: Workout[]){
    this.addedWorkouts.emit(workoutsFromProgram);
  }
}
