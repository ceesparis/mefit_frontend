import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Exercise } from 'src/app/models/exercise.model';
import { Workout } from 'src/app/models/workout.model';

@Component({
  selector: 'app-workout-selector',
  templateUrl: './workout-selector.component.html',
  styleUrls: ['./workout-selector.component.scss']
})
export class WorkoutSelectorComponent implements OnInit {

  @Output() public addedWorkout= new EventEmitter<Workout>();
  @Input() workouts: Workout[] = [];
  @Input() workout: any;

  constructor() { }

  ngOnInit(): void {
  }

  onAddToGoal(workoutToBeAdded: Workout){
    this.addedWorkout.emit(workoutToBeAdded);
  }

}
