import { NonNullAssert } from '@angular/compiler';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { WorkoutType } from 'src/app/enums/workout-type';
import { Exercise } from 'src/app/models/exercise.model';
import { WorkoutExercise } from 'src/app/models/workout-exercise.model';
import { Workout } from 'src/app/models/workout.model';
import { ExerciseService } from 'src/app/services/exercise.service';
import { WorkoutService } from 'src/app/services/workout.service';

@Component({
  selector: 'app-custom-workout-creator',
  templateUrl: './custom-workout-creator.component.html',
  styleUrls: ['./custom-workout-creator.component.scss']
})
export class CustomWorkoutCreatorComponent implements OnInit {

  @Input() selectedExercise?: Exercise;
  @Output() public CustomWorkoutEvent = new EventEmitter<Workout>();
  customWorkout?: Workout;

  get exercises(): Exercise[] {
    return this.exerciseService.exercises;
  }

  setsControl = new FormControl(0);
  repetitionsControl = new FormControl(0);

  constructor( private fb: FormBuilder, private exerciseService: ExerciseService) {
   }

   addExerciseForm = this.fb.group({
    exercise: ["", [Validators.required]],
  });

  ngOnInit(): void {
    this.exerciseService.getAllExercises();
  }


  addToCustomWorkout() {
    if (!this.selectedExercise) return

    if (this.customWorkout) {
      const newWorkoutExercise: WorkoutExercise = this.createCustomWorkout(this.selectedExercise);
      this.customWorkout.exercises.push(newWorkoutExercise);
    } else {
      const newWorkoutExercise: WorkoutExercise = this.createCustomWorkout(this.selectedExercise);
      this.customWorkout = { id: 1, creatorId: 1, exercises: [newWorkoutExercise], name: "Custom Workout", type: WorkoutType.BALANCE };
    }

  }
  createCustomWorkout(exercise: Exercise) {
    const newWorkoutExercise: WorkoutExercise = {
      id: exercise.id,
      lastEditorId: exercise.lastEditorId,
      creatorId: exercise.creatorId, name: exercise.name,
      targetMuscleGroups: exercise.targetMuscleGroups, description: exercise.description,
      repetitions: this.repetitionsControl.value, sets: this.setsControl.value
    }
    return newWorkoutExercise
  }

  addCustomWorkout(){
    if (this.customWorkout) this.CustomWorkoutEvent.emit(this.customWorkout);
  }

  deleteExerciseCustomWorkout(workoutExercise: WorkoutExercise){
    if(this.customWorkout){
      const index: number = this.customWorkout.exercises.indexOf(workoutExercise);
      this.customWorkout.exercises.splice(index, 1);
    }
  }
}
