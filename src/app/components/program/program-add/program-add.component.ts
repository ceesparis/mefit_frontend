import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { TooltipPosition } from '@angular/material/tooltip';
import { Program } from 'src/app/models/program.model';
import { ProgramService } from 'src/app/services/program.service';

@Component({
  selector: 'app-program-add',
  templateUrl: './program-add.component.html',
  styleUrls: ['./program-add.component.scss']
})
export class ProgramAddComponent implements OnInit{

  // @Cees discuss with Markus if we need this at all, or if it is better to make use of the service in here to get available programs
  //@Input() programs: Program[] = [] // allows passing data from a parent component on initialization of this component
  
  @Output() public selectedProgramEvent = new EventEmitter<Program>();

  constructor(private fb: FormBuilder, private readonly programService: ProgramService) { }

  get programs(): Program[]{
    return this.programService.programs;
  }

  ngOnInit(): void{
    this.programService.getAllPrograms()
  }

  addProgramForm = this.fb.group({
    program: [undefined, [Validators.required]],
  });

  get program(){return this.addProgramForm.get('program')}

  addProgram(){
    if (this.program) this.selectedProgramEvent.emit(this.program.value)
  }

   // for tooltip
   positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
}
