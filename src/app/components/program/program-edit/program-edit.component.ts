import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ProgramCategory } from 'src/app/enums/program-category';
import { Workout } from 'src/app/models/workout.model';
import { ProgramService } from 'src/app/services/program.service';
import { NewProgram } from 'src/app/models/program.model';
import { HttpErrorResponse } from '@angular/common/http';
import { TooltipPosition } from '@angular/material/tooltip';
import { StorageUtil } from 'src/app/utils/storage.util';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';

// requires the child component app-workout-add, to add workouts to the program 
@Component({
  selector: 'app-program-edit',
  templateUrl: './program-edit.component.html',
  styleUrls: ['./program-edit.component.scss'],
})
export class ProgramEditComponent {
  
  constructor(private fb: FormBuilder, private programService: ProgramService) {}
  
  // gets available program categories from the programService
  programCategoryValues = Object.keys(ProgramCategory).filter((v) => isNaN(Number(v)));


  // selected workouts, which should be added to the program
  addedWorkouts: Workout[] = [];

  get workoutIds(): number[]{
    return this.addedWorkouts.map(workout => workout.id)
  }

  // definition of reactive form for basic program information
  programBasicsForm = this.fb.group({
    name: ['', [Validators.required]],
    category: ['', [Validators.required]],
    description: ['', [Validators.required]],
  });

  // getters for form values
  get name() {
    return this.programBasicsForm.get('name');
  }
  get category() {
    return this.programBasicsForm.get('category');
  }
  get description(){
    return this.programBasicsForm.get('description');
  }

  // when a workout is added in the add-workout component (= child of this component) this method is called
  addWorkout(workout: Workout){
    this.addedWorkouts.push(workout);
  }

  // remove Workout from addedWorkouts when Delete button in DragDrop element is clicked
  removeWorkoutFromProgram(programWorkout: Workout) {
    this.addedWorkouts = this.addedWorkouts.filter(
      (pw) => pw !== programWorkout
    );
  }

  // sorting addedWorkouts based on changed order in DragDrop element
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(
      this.addedWorkouts,
      event.previousIndex,
      event.currentIndex
    );
  }

  // initiates an POST/PUT/PATCH via the program-service to the API
  saveProgram() {
    // 1. POST new Program
    const newProgram: NewProgram = {
      name: this.name?.value,
      category: this.category?.value,
      description: this.description?.value,
      lastEditorId: 0, // TODO adjust
      creatorId: 0 // TODO adjust
    }
    this.programService.addProgram(newProgram)
    .subscribe({
      next: (response: string) => {
        const programId: number = Number.parseInt(response.replace('programs/',''))
        this.addWorkoutsToProgram(programId)
      },
      error: (error: HttpErrorResponse) => {
        this.programService.error = error.message;
        this.manageDisplayError()
      },
    })
  }

  addWorkoutsToProgram(programId: number) {
    this.programService.addWorkoutsToProgram(this.workoutIds, programId)
    .subscribe({
      next: () => {
        StorageUtil.storageRemove(StorageKeys.Programs)
        this.manageDisplaySuccess()        
      },
      error: (error: HttpErrorResponse) => {
        this.programService.error = error.message;
        this.manageDisplayError()
      },
    })
  }

  private _displaySuccess: boolean = false
  private _displayError: boolean = false

  // UI indication of REST-Communication progress/result
  get loading(): boolean{
    return this.programService.loading
  }
  get displaySuccess(): boolean{
    return this._displaySuccess;
  }
  get displayError(): boolean{
    return this._displayError;
  }
  manageDisplaySuccess(): void {    
    this._displaySuccess = true;
    setTimeout(()=>{
      this._displaySuccess = false
    },3000)
  }
  manageDisplayError(): void {    
    this._displayError = true;
    setTimeout(()=>{
      this._displayError = false
    },6000)
  }

    // for tooltip
    positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];

}
