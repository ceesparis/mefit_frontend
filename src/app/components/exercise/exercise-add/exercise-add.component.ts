import { Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { TooltipPosition } from '@angular/material/tooltip';
import { Exercise } from 'src/app/models/exercise.model';
import { WorkoutExercise } from 'src/app/models/workout-exercise.model';
import { ExerciseService } from 'src/app/services/exercise.service';

@Component({
  selector: 'app-exercise-add',
  templateUrl: './exercise-add.component.html',
  styleUrls: ['./exercise-add.component.scss'],
})
export class ExerciseAddComponent implements OnInit {
  
  // to emit the WorkoutExercise that should be added to the parent component of this component
  @Output() public selectedWorkoutExerciseEvent = new EventEmitter<WorkoutExercise>();
  
  constructor(private fb: FormBuilder, private exerciseService: ExerciseService) {}

  get exercises(): Exercise[]{
    return this.exerciseService.exercises
  }

  ngOnInit(): void{
    this.exerciseService.getAllExercises()
  }

  // get available exercises from exerciseService
  // availableExercises: Exercise[] = this.exerciseService.exercises

  // form for setting up a workoutExercise
  workoutExerciseForm = this.fb.group({
    exercise: ['', [Validators.required]],
    sets: [
      '',
      [
        Validators.required,
        Validators.min(1),
        Validators.pattern('[1-9]+[0-9]*'),
      ],
    ],
    repetitions: [
      '',
      [
        Validators.required,
        Validators.min(1),
        Validators.pattern('[1-9]+[0-9]*'),
      ],
    ],
  });
  
  // getters for form values
  get exercise() {
    return this.workoutExerciseForm.get('type');
  }
  get sets() {
    return this.workoutExerciseForm.get('sets');
  }
  get repetitions() {
    return this.workoutExerciseForm.get('repetitions');
  }

  // creates an WorkoutExercise based on provided form values
  get workoutExercise(){
    const formValue: any = this.workoutExerciseForm.value;
    const exercise: Exercise = formValue.exercise; // TODO check
    const workoutExercise: WorkoutExercise = {
      id: exercise.id,
      lastEditorId: exercise.lastEditorId,
      creatorId: exercise.creatorId,
      name: exercise.name,
      targetMuscleGroups: exercise.targetMuscleGroups,
      description: exercise.description,
      sets: formValue.sets,
      repetitions: formValue.repetitions,
    };
    console.warn('WorkoutExercise: ', workoutExercise)
    return workoutExercise    
  }

  // emits the created WorkoutExercise via an Event emitter to parent components
  addWorkoutExercise(){
    // TODO check if emitted value is what we actually want to emit
    if (this.workoutExercise) this.selectedWorkoutExerciseEvent.emit(this.workoutExercise)
  }

  // for tooltip
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];

}
