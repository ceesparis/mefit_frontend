import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MuscleGroup } from 'src/app/enums/muscle-group';
import { Exercise, NewExercise } from 'src/app/models/exercise.model';
import { ExerciseService } from 'src/app/services/exercise.service';
import { TooltipPosition } from '@angular/material/tooltip';
import { StorageUtil } from 'src/app/utils/storage.util';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';

@Component({
  selector: 'app-exercise-edit',
  templateUrl: './exercise-edit.component.html',
  styleUrls: ['./exercise-edit.component.scss'],
})
export class ExerciseEditComponent implements OnInit {
  muscleGroupValues = Object.keys(MuscleGroup).filter((v) => isNaN(Number(v)));

  constructor(
    private readonly fb: FormBuilder,
    private readonly exerciseService: ExerciseService
  ) {}

  ngOnInit(): void {
    console.log('ExerciseForm on initialization: ',this.exerciseForm)
  }

  get exercisePictureUrl(): string {
    const placeholderLink: string =
      'https://www.worldartfoundations.com/wp-content/uploads/2022/04/placeholder-image.png';

    if (this.image === null) {
      return placeholderLink;
    }
    let content: string = this.image.value;
    if (content === null) {
      return placeholderLink;
    }
    if (content.length === 0) {
      return placeholderLink;
    }
    return this.image?.value;
  }

  exerciseForm = this.fb.group({
    name: ['', [Validators.required]],
    description: ['', [Validators.required]],
    targetMuscleGroup: ['', [Validators.required]],
    image: ['', [Validators.required]],
    videoLink: [''],
  });

  get name() {
    return this.exerciseForm.get('name');
  }
  get description() {
    return this.exerciseForm.get('description');
  }
  get targetMuscleGroup() {
    return this.exerciseForm.get('targetMuscleGroup');
  }
  get image() {
    return this.exerciseForm.get('image');
  }
  get videoLink() {
    return this.exerciseForm.get('videoLink');
  }

  submitExerciseForm() {
    // create new Exercise
    const newExercise: NewExercise = {
      creatorId: 0, // TODO set loggedInUserId
      lastEditorId: 0, // TODO set loggedInUserId
      description: this.description?.value,
      imageUrl: this.image?.value,
      name: this.name?.value,
      targetMuscleGroups: this.targetMuscleGroup?.value,
      videoUrl: this.videoLink?.value,
    };
    this.exerciseService.addExercise(newExercise).subscribe({
      next: () => {
        StorageUtil.storageRemove(StorageKeys.Exercises);
        this.manageSuccess();
      },
      error: (error: HttpErrorResponse) => {
        this.exerciseService.error = error.message;
        this.manageDisplayError();
      },
    });
  }

  private _displaySuccess: boolean = false;
  private _displayError: boolean = false;

  get loading(): boolean {
    return this.exerciseService.loading;
  }

  get displaySuccess(): boolean {
    return this._displaySuccess;
  }

  get displayError(): boolean {
    return this._displayError;
  }

  manageSuccess(): void {
    this._displaySuccess = true;
    setTimeout(() => {
      this._displaySuccess = false;
      // Reset the form
      //this.resetForm(this.exerciseForm);
    }, 3000);
  }

  resetForm(form: FormGroup) {
    form.reset();
    form.markAsUntouched();
    Object.keys(form.controls).forEach((key) => {
      //form.get(key)?.setErrors(null);
      form.get(key)?.markAsUntouched()
      form.get(key)?.reset()
    });
    console.log('ExerciseForm after reset: ',this.exerciseForm)
  }

  manageDisplayError(): void {
    this._displayError = true;
    setTimeout(() => {
      this._displayError = false;
    }, 6000);
  }

  // for tooltip
  positionOptions: TooltipPosition[] = [
    'after',
    'before',
    'above',
    'below',
    'left',
    'right',
  ];
}
