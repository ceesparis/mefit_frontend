export enum StorageKeys {
    User = "user",
    Exercises = "exercise-catalogue",
    Workouts = "workout-catalogue",
    Programs = "program-catalogue",
    Goal = "goal"

}