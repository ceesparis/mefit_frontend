export enum MuscleGroup{
    CHEST,
    BACK,
    ARMS,
    ABDOMINAL,
    LEGS,
    SHOULDERS,
    TRICEPS,
    BICEPS
}
