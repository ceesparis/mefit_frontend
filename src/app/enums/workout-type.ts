export enum WorkoutType{
    ENDURANCE,
    STRENGTH,
    BALANCE,
    FLEXIBILITY,
    CUSTOM
}