import { Directive } from "@angular/core"
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from "@angular/forms"

export const passwordsDoNotMatchValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const password = control.get('password')
    const confirmPassword = control.get('confirmPassword')

    return password && confirmPassword && password.value !== confirmPassword.value ? {
      passwordsDoNotMatch: true} : null
  }

@Directive({
    selector: '[appPasswordsDoNotMatch]',
    providers: [{ provide: NG_VALIDATORS, useExisting: PasswordsDoNotMatchDirective, multi: true}]
})
export class PasswordsDoNotMatchDirective implements Validator {
    validate(control: AbstractControl): ValidationErrors | null {
        return passwordsDoNotMatchValidator(control);
    }
}