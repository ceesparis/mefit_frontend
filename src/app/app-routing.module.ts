import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardPage } from './pages/dashboard/dashboard.page';
import { ExerciseCataloguePage } from './pages/exercise-catalogue/exercise-catalogue.page';
import { LoginPage } from './pages/login/login.page';
import { ProgramCataloguePage } from './pages/program-catalogue/program-catalogue.page';
import { WorkoutCataloguePage } from './pages/workout-catalogue/workout-catalogue.page';
import { ProfilePage } from './pages/profile/profile.page';
import { RegisterPage } from './pages/register/register.page';
import { ContributionPage } from './pages/contribution/contribution.page';
import { CataloguePage } from './pages/catalogue/catalogue.page';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
  { path: 'exercise', component: ExerciseCataloguePage, canActivate:[AuthGuard] },
  { path: 'workout', component: WorkoutCataloguePage, canActivate:[AuthGuard] },
  { path: 'program', component: ProgramCataloguePage, canActivate:[AuthGuard] },
  { path: 'profile', component: ProfilePage, canActivate:[AuthGuard] },
  { path: 'register', component: RegisterPage, canActivate:[AuthGuard] },
  { path: 'contribution', component: ContributionPage, canActivate:[AuthGuard] },
  { path: 'dashboard', component: DashboardPage, canActivate:[AuthGuard] },
  { path: 'catalogue', component: CataloguePage, canActivate:[AuthGuard] },
  { path: 'login', component: LoginPage},
  { path: '', component: LoginPage, pathMatch:"full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
