import { Component, OnInit } from '@angular/core';
import { ProgramCategory } from 'src/app/enums/program-category';
import { Program } from 'src/app/models/program.model';
import { ProgramService } from 'src/app/services/program.service';

@Component({
  selector: 'app-program-catalogue',
  templateUrl: './program-catalogue.page.html',
  styleUrls: ['./program-catalogue.page.scss']
})
export class ProgramCataloguePage implements OnInit {

  get programs(): Program[]{
    return this.programService.programs;
  }

  constructor(
    private readonly programService: ProgramService
  ) { }

  ngOnInit(): void {
    this.programService.getAllPrograms();
  }

  filters: ProgramCategory[] = [];
  filterOptions: string[] = ["EASY", "INTERMEDIATE", "INTENSE"];

  selectedFilters(filters: ProgramCategory[]){
    this.filters = filters;
  }

}
