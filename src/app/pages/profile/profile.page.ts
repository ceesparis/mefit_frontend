import { Component, OnInit } from '@angular/core';
import { Profile } from 'src/app/models/profile.model';
import { ProfileService } from 'src/app/services/profile.service';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  
  profileId: number = 1; // TODO should be dynamically based on logged-in user
  
  get loading(): boolean {
    return this.profileService.loading
  }

  get profile(): Profile {
    return this.profileService.profile
  }

  get error(): string {
    return this.profileService.error
  }

  constructor(
    private readonly profileService: ProfileService
  ) {}

  ngOnInit(): void {
    this.profileService.getProfileById(this.profileId);
  }

}
