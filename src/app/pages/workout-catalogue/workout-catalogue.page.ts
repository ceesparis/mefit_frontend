import { Component, OnInit } from '@angular/core';
import { WorkoutType } from 'src/app/enums/workout-type';
import { Workout } from 'src/app/models/workout.model';
import { WorkoutService } from 'src/app/services/workout.service';

@Component({
  selector: 'app-workout-catalogue',
  templateUrl: './workout-catalogue.page.html',
  styleUrls: ['./workout-catalogue.page.scss']
})
export class WorkoutCataloguePage implements OnInit {

  get workouts(): Workout[] {
    return this.workoutService.workouts;
  }

  constructor(
    private readonly workoutService: WorkoutService
  ) { }

  ngOnInit(): void {
    this.workoutService.getAllWorkouts();
  }

  filters: WorkoutType[] = [];
  filterOptions: string[] = ["FLEXIBILITY", "BALANCE", "STRENGTH", "ENDURANCE",  "CUSTOM"];

  selectedFilters(filters: WorkoutType[]){
    this.filters = filters;
  }

}
