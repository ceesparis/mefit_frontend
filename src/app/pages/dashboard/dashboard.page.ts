import { Component, OnInit } from '@angular/core';
import { Exercise } from 'src/app/models/exercise.model';
import { Goal } from 'src/app/models/goal.model';
import { GoalWorkout } from 'src/app/models/goal-workout.model'
import { Program } from 'src/app/models/program.model';
import { Workout } from 'src/app/models/workout.model';
import { ExerciseService } from 'src/app/services/exercise.service';
import { ProgramService } from 'src/app/services/program.service';
import { WorkoutService } from 'src/app/services/workout.service';
import { GoalService } from 'src/app/services/goal.service';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { ProfileService } from 'src/app/services/profile.service';
import { KeycloakService } from 'src/app/services/keycloak.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss']
})
export class DashboardPage implements OnInit {

  currentGoal?: Goal;
  goals: Goal[] = [];
  addedWorkouts: Workout[] = [];
  progress: number = 0;
  daysLeft: number = 0;


  get exercises(): Exercise[] {
    return this.exerciseService.exercises;
  }

  get workouts(): Workout[] {
    return this.workoutService.workouts
  }

  get programs(): Program[] {
    return this.porgramService.programs
  }

  constructor(
    private readonly exerciseService: ExerciseService,
    private readonly workoutService: WorkoutService,
    private readonly porgramService: ProgramService,
    private readonly goalService: GoalService,
    private readonly profileService: ProfileService,
    private readonly userService: UserService,
    private readonly keycloakService: KeycloakService,
    private readonly loginService: LoginService,

  ) { }

  get profileId(): number|null{
    if (this.userService.user) return this.userService.user.profileId
    return null
  }

  ngOnInit(): void {
    this.loginService.findUserByKeycloakId();
    console.log(this.keycloakService.getNameUser());
    if(this.profileId){
      this.profileService.getGoalsIdProfileById(this.profileId);
    }
    const storedGoal = this.goalService.getGoal();
    if (storedGoal != undefined){
      this.currentGoal = storedGoal;
    } 
    this.determineDaysLeft();
    this.determineProgress();
  }

  addWorkout(workout: Workout) {
    this.addedWorkouts.push(workout);
  }

  addWorkouts(workouts: Workout[]) {
    workouts.forEach(w => this.addedWorkouts.push(w));
  }

  // deletes workouts already in goal
  deleteWorkout(workout: Workout) {
    const index: number = this.addedWorkouts.indexOf(workout);
    this.addedWorkouts.splice(index, 1);
  }

  createGoal() {
    const goalWorkouts: GoalWorkout[] = [];

    //get the current date
    const date: string = this.getDate();

    // convert workouts to GoalWorkouts
    for (const workout of this.addedWorkouts) {
      const newGoalWorkout: GoalWorkout = this.convertWorkoutToGoalWorkout(workout);
      goalWorkouts.push(newGoalWorkout);
    }

    if (goalWorkouts.length == 0){
      alert('you have to add workouts to the goal before you can create it!')
      return
    } 
    var profId = 1;
    if (this.profileId != null) profId = this.profileId;
    // create a goal
    const goal: Goal = { id: 0, profileId: profId, workouts: goalWorkouts, pendingWorkouts: goalWorkouts, completedWorkouts: [], startDate: date }

    this.goalService.createGoal(goal);
    this.goals.push(goal);
    this.currentGoal = goal;
    this.determineDaysLeft();
    this.addedWorkouts = [];
  }


  convertWorkoutToGoalWorkout(workout: Workout) {
    const newGoalWorkout: GoalWorkout = {
      id: workout.id, lastEditorId: workout.lastEditorId,
      creatorId: workout.creatorId, name: workout.name,
      type: workout.type, exercises: workout.exercises,
      description: workout.description, completed: false
    }
    return newGoalWorkout
  }

  getDate() {
    console.log("new date gotten")
    const today: Date = new Date();
    const year: string = this.formatDate(today.getFullYear());
    const month: string = this.formatDate(today.getMonth() + 1);
    const day: string = this.formatDate(today.getDate());
    return year + '-' + month + '-' + day;

  }

  formatDate(dateElement: number) {
    if (dateElement < 10) return "0" + dateElement;
    return dateElement.toString();
  }

  completeWorkout(workout: Workout) {
    if (this.currentGoal) {
      const pendingWorkout: Workout|undefined = this.currentGoal.pendingWorkouts.find(x => x.id == workout.id);
      if (pendingWorkout){
        this.goalService.updateWorkout(this.currentGoal, workout);
      this.currentGoal.completedWorkouts.push(workout);
      this.currentGoal.pendingWorkouts = this.currentGoal.pendingWorkouts.filter(w => w != pendingWorkout);
      }
      this.determineProgress();
      if (this.currentGoal.pendingWorkouts.length == 0) {
        setTimeout(() => this.completeGoal(), 500);
      }
    }
  }

  determineProgress(): void {
    if (this.currentGoal){
      this.progress =  this.currentGoal.completedWorkouts.length/this.currentGoal.workouts.length * 100
    }
  }

  completeGoal(): void {
    alert("Congratulations! You finished your Goal!");
    this.currentGoal = undefined;
    this.progress = 0;
    this.goalService.removeGoalFromSessionStorage();
  }

  determineDaysLeft(): void {
    if(this.currentGoal){
      const year = this.currentGoal.startDate.slice(0, 4);
      const month = this.currentGoal.startDate.slice(5, 7);
      const day =  this.currentGoal.startDate.slice(8, 10);
      const endDate = new Date(`${month}/${day}/${year}`);
      endDate.setDate(endDate.getDate()+7)
      const currentDate =  new Date();
      const difference =   endDate.getTime() - currentDate.getTime();
      const totalDays = Math.ceil(difference / (1000 * 3600 * 24));
      this.daysLeft = totalDays;
    } else{
      console.log("no current goal")
    }
  }


}
