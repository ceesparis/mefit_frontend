import { Component, OnInit } from '@angular/core';
import { MuscleGroup } from 'src/app/enums/muscle-group';
import { Exercise } from 'src/app/models/exercise.model';
import { ExerciseService } from 'src/app/services/exercise.service';

@Component({
  selector: 'app-exercise-catalogue',
  templateUrl: './exercise-catalogue.page.html',
  styleUrls: ['./exercise-catalogue.page.scss']
})
export class ExerciseCataloguePage implements OnInit {

  get exercises(): Exercise[] {
    return this.exerciseService.exercises;
  }


  constructor(
    private readonly exerciseService: ExerciseService
  ) { }

  ngOnInit(): void {
    this.exerciseService.getAllExercises()
  }

  filters: MuscleGroup[] = [];
  filterOptions: string[] = Object.keys(MuscleGroup).filter((v) => isNaN(Number(v)));

  selectedFilters(filters: MuscleGroup[]){
    this.filters = filters;
  }

}
