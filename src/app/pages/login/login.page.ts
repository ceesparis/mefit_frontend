import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import keycloak from 'src/keycloak';
import { KeycloakService } from 'src/app/services/keycloak.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  myimage:string = "assets/images/MeFit.png"
  loading: boolean = false;
  @Output() login: EventEmitter<void> = new EventEmitter();
  userService: any;
  loginService: any;

  // constructor( private fb: FormBuilder,
  //    private loginService: LoginService,
  //     private userService: UserService,
  //     private router: Router) { }

   loginForm = this.fb.group({
     username: ['', [Validators.required]],
     password: ['', [Validators.required]],
   });

  // ngOnInit(): void {
  //   if(this.userService.user) this.router.navigateByUrl("/dashboard");
  // }

  // got from angular-keycloak example
  get authenticated(): boolean {
    return Boolean(keycloak.authenticated);
  }

  constructor(
    private router: Router,
    private keycloakService: KeycloakService,
    private fb: FormBuilder,
    ) { }

  ngOnInit(): void {
    if (keycloak.authenticated) {
      this.router.navigateByUrl("/dashboard");
    }
  }

  doLogin(): void {
    keycloak.login();
  }


  }
  // end of quote


  // loginSubmit(){
  //   console.log("loginsubmit")
  //   const username = this.loginForm.get("username")?.value;
  //   const id = 1;
  //   // const password = this.loginForm.get("password")?.value;
  //   this.loading=true;
  //   this.loginService.login(username, id)
  //   .subscribe({
  //     next: (user: User) => {
  //       this.userService.user = user;
  //       this.login.emit();
  //       this.loading = false;
  //       this.router.navigateByUrl("/dashboard")
  //     },
  //     error: () => {
  //       this.loading = false;
  //     }
  //   })
  // }


