import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { passwordsDoNotMatchValidator } from 'src/app/utils/passwords-do-not-match.directive';

@Component({
  selector: 'app-register-page',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  
  passwordMinlength: number = 8

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  // Using FormBuilder to create credentialsForm
  // TODO improve Error highlighting of the cross-field validation. Good example: https://stackblitz.com/run?file=src%2Fapp%2Freactive%2Fhero-form-reactive.component.html
  credentialsForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(this.passwordMinlength)]],
    confirmPassword: ['', [Validators.required]],
  }, {validators: passwordsDoNotMatchValidator})

  submitCredentialsForm(){
    console.warn(this.credentialsForm.value)
  }

  // Getters to simplify access in template for, e.g., *ngIf
  get email() {return this.credentialsForm.get('email')}
  get password() {return this.credentialsForm.get('password')}
  get confirmPassword() {return this.credentialsForm.get('confirmPassword')}

}