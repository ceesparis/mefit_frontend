import { Injectable } from '@angular/core';
import { Workout } from '../models/workout.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from "src/environments/environment";
import { finalize } from 'rxjs';
import { StorageUtil } from '../utils/storage.util';
import { StorageKeys } from '../enums/storage-keys.enum';

const { backendAPI, workoutsEndpoint} = environment;

@Injectable({
  providedIn: 'root'
})
export class WorkoutService {
 
  constructor(private readonly http: HttpClient) { }
  private _workouts: Workout[] = [];
  private _error: string = "";
  private _loading: boolean = false;
  
  get workouts(): Workout[] {
      return this._workouts
  }

  get loading(): boolean {
      return this._loading;
  }

  get error(): string {
      return this._error;
  }

  public getAllWorkouts(): void {

    if (this._workouts.length > 0 || this.loading) return
    
    this._loading = true;
    const localStoredWorkouts = StorageUtil.storageRead<Workout[]>(StorageKeys.Workouts);
    if (localStoredWorkouts !== undefined) {
      this._workouts = localStoredWorkouts;
      this._loading = false;
      return
    }

      this.http.get<Workout[]>(`${backendAPI}${workoutsEndpoint}`)
        .pipe(
          finalize(() => {
            this._loading = false;
          })
        )
        .subscribe({
          next: (data: Workout[]) => {
             this._workouts = [...data];
             StorageUtil.storageSave<Workout[]>(StorageKeys.Workouts, this._workouts);
            },
          error: (error: HttpErrorResponse) => {
            this._error = error.message;
            console.log(error.message);
          }
        })
  }
 
}
