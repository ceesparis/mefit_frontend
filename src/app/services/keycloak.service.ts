import { Injectable } from '@angular/core';
import keycloak from 'src/keycloak';

@Injectable({
  providedIn: 'root'
})
export class KeycloakService {

  constructor() { }

  getIdUser(): string|undefined{
    if (keycloak.tokenParsed && keycloak.tokenParsed.sid){
      return keycloak.tokenParsed.sid
    }
    return undefined
  }

  getNameUser(): string | undefined{
    if (keycloak.tokenParsed && keycloak.tokenParsed.preferred_username){
      return keycloak.tokenParsed.preferred_username
    }
    return undefined
  }
}
