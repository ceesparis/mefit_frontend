import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map, switchMap, of, finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';
import { KeycloakService } from './keycloak.service';
import { ProfileService } from './profile.service';
import { UserService } from './user.service';


const {backendAPI, usersEndpoint , apiKey } = environment

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private readonly http: HttpClient,
     private profileService: ProfileService,
     private keycloakService: KeycloakService,
     private userService: UserService) { }

  public login(id: string, username: string): Observable<User> {
    return this.checkUsername(username)
      .pipe(
        switchMap((user: User | null) => {
          if (user === null) {
            return this.createUser(username, id)
          }
          return of(user);
        })
      )
  }

  private checkUsername(username: string): Observable<User | null> {
    return this.http.get<User>(`${backendAPI}${usersEndpoint}/${username}`)
      .pipe(
        map((response: User) => response)
      )
  }

  private createUser(username: string, id: string): Observable<User> {
    const user = {
      "userName": username,
      "id": id,
    };

    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey,
    });

    return this.http.post<User>(`${backendAPI}${usersEndpoint}`, user, {
      headers
    })
    .pipe(
      finalize(() =>{
        this.profileService.createProfile(this.profileService.profile);
      })
    );
  }

  public findUserByKeycloakId(){
    const username = this.keycloakService.getNameUser();
    const id = this.keycloakService.getIdUser();
    if (id != undefined && username !=undefined )this.login(id, username)
    .subscribe({
      next: (user: User) => {
        this.userService.user = user;
        // this.login.emit();
  
      },
      error: () => {
        console.log("sth went wrong")
      }
    })
  }


}
