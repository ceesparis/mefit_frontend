import { HttpHeaders, HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Goal } from '../models/goal.model';
import { environment } from "src/environments/environment";
import { finalize } from 'rxjs';
import { Workout } from '../models/workout.model';
import { StorageUtil } from '../utils/storage.util';
import { StorageKeys } from '../enums/storage-keys.enum';
import { AnyForUntypedForms } from '@angular/forms';
import { ProfileService } from './profile.service';
import { UserService } from './user.service';
import { GoalWorkout } from '../models/goal-workout.model';
import { WorkoutService } from './workout.service';

const { backendAPI, goalEndpoint, apiKey, workoutsEndpoint } = environment;

@Injectable({
  providedIn: 'root'
})
export class GoalService {

  private _error: string = "";
  private _goalId: number = 0;
  private goal?: Goal;


  constructor(private readonly http: HttpClient,
    private readonly userService: UserService,
    private readonly workoutService: WorkoutService) { }

  getGoal(): Goal | undefined {
    const localStoredGoal = StorageUtil.storageRead<Goal>(StorageKeys.Goal);
    if (localStoredGoal !== undefined) {
      return localStoredGoal
    }
    if (this.userService.user) {
      // this.profileService.getGoalsIdProfileById(this.userService.user.profileId)
    }
    return undefined
  }

  removeGoalFromSessionStorage() {
    StorageUtil.storageRemove(StorageKeys.Goal);
  }

  getId() {
    return this._goalId;
  }


  public createGoal(goal: Goal) {
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey,
    });

    const goalInitValues = {
      startDate: goal.startDate,
      profileId: goal.profileId
    }
    this.http.post<any>(`${backendAPI}${goalEndpoint}`, goalInitValues, {
      headers: headers, observe: "response"
    }).pipe(
      finalize(() => {
        this.addWorkoutsToGoal(goal);
        StorageUtil.storageSave<Goal>(StorageKeys.Goal, goal);
      })
    )
      .subscribe(res => {
        const id: number = parseInt(res.body.toString().match(/(\d+)/))
        goal.id = id;
        this._goalId = id;
      })
  }



  public addWorkoutsToGoal(goal: Goal) {
    for (const workout of goal.workouts) {
      this.addWorkoutToGoal(goal.id, workout.id);
    }
  }

  public addWorkoutToGoal(goalId: number, workoutId: number) {

    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey,
    });

    return this.http.put<any>(`${backendAPI}${goalEndpoint}/${goalId}/${workoutsEndpoint}`, workoutId, {
      headers: headers
    })
      .subscribe({
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
          console.log(error.message);
        }
      })
  }

  public updateWorkout(goal: Goal, workout: Workout) {
    this.http.get<any>(`${backendAPI}${goalEndpoint}/${goal.id}`)
      .pipe(
        finalize(() => {
          StorageUtil.storageSave<Goal>(StorageKeys.Goal, goal);
        })
      )
      .subscribe(res => {
        for (const workoutDB of res.workouts) {
          if (workoutDB.workoutId == workout.id && !workoutDB.done) {
            this.updateSpecificWorkout(workoutDB.workoutGoalId);
          }
        }
      })
  }

  updateSpecificWorkout(workoutGoalId: number) {
    return this.http.put(`${backendAPI}${goalEndpoint}/${workoutGoalId}`, true)
      .subscribe({
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      })
  }

  getGoalById(id: number) {
    this.http.get<Goal>(`${backendAPI}${goalEndpoint}/${id}`)
      .subscribe(res => {
        console.log(res)
        const goalWorkouts: any[] = res.workouts
        const allWorkouts: Workout[] | undefined = StorageUtil.storageRead(StorageKeys.Workouts);
        var completeWorkouts: GoalWorkout[] = [];
        for (const goalWorkout of goalWorkouts) {
          var workout: Workout | undefined;
          if (allWorkouts != undefined) workout = allWorkouts.filter(w => w.id == goalWorkout.workoutId).pop();
          if (workout != undefined) {
            const newWorkout: GoalWorkout = {
              lastEditorId: workout.lastEditorId, creatorId: workout.creatorId,
              name: workout.name, type: workout.type, exercises: workout.exercises,
              description: workout.description, id: goalWorkout.workoutGoalId, workoutId: workout.id,
              completed: goalWorkout.done
            }
            completeWorkouts.push(newWorkout)
          }
        }
        res.workouts = completeWorkouts;
        res.completedWorkouts = completeWorkouts.filter(w => w.completed == true)
        res.pendingWorkouts = completeWorkouts.filter(w => w.completed == false)
        StorageUtil.storageSave(StorageKeys.Goal, res)
      })

  }



}
