import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import {
  Exercise,
  ExerciseResponse,
  NewExercise,
} from '../models/exercise.model';
import { pipe, finalize } from 'rxjs';
import { StorageUtil } from '../utils/storage.util';
import { StorageKeys } from '../enums/storage-keys.enum';

const { backendAPI, exerciseEndpoint, apiKey } = environment;
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'x-api-key': apiKey,
  }),
};



@Injectable({
  providedIn: 'root',
})
export class ExerciseService {
  constructor(private readonly http: HttpClient) {}
  private _exercises: Exercise[] = [];
  private _exercisesResponse: ExerciseResponse[] = [];
  private _error: string = '';
  private _loading: boolean = false;

  get exercises(): Exercise[] {
    return this._exercises;
  }

  get exercisesResponse(): ExerciseResponse[] {
    return this._exercisesResponse;
  }

  get loading(): boolean {
    return this._loading;
  }

  get error(): string {
    return this._error;
  }

  set error(message: string) {
    this._error = message;
  }

  public getAllExercises(): void {

    if (this._exercises.length > 0 || this.loading) return
    
    this._loading = true;
    const localStoredExercises = StorageUtil.storageRead<Exercise[]>(StorageKeys.Exercises);
    if (localStoredExercises !== undefined) {
      this._exercises = localStoredExercises;
      this._loading = false;
      return
    }
    this.http
      .get<Exercise[]>(`${backendAPI}${exerciseEndpoint}`)
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (data: Exercise[]) => {
          this._exercises = [...data];
          StorageUtil.storageSave<Exercise[]>(StorageKeys.Exercises, this._exercises);
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
          console.log(error.message);
        },
      });
  }

  public getExercisesResponse(): Observable<ExerciseResponse[]> {
    this._loading = true;

    return this.http.get<ExerciseResponse[]>(`${backendAPI}${exerciseEndpoint}`).pipe(
      finalize(() => {
        this._loading = false;
      })
    );
  }

  public addExercise(newExercise: NewExercise): Observable<Exercise> {
    return this.http.post<Exercise>(
      `${backendAPI}${exerciseEndpoint}`,
      newExercise,
      httpOptions
    );
  }
}
