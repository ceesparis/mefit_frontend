import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NewProgram, Program } from '../models/program.model';
import { environment } from 'src/environments/environment';
import { finalize, Observable } from 'rxjs';
import { StorageUtil } from '../utils/storage.util';
import { StorageKeys } from '../enums/storage-keys.enum';

const { backendAPI, programsEndpoint, apiKey } = environment;
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'x-api-key': apiKey,
  }),
};

@Injectable({
  providedIn: 'root',
})
export class ProgramService {
  constructor(private readonly http: HttpClient) {}
  private _programs: Program[] = [];
  private _error: string = '';
  private _loading: boolean = false;

  get programs(): Program[] {
    return this._programs;
  }

  get loading(): boolean {
    return this._loading;
  }

  get error(): string {
    return this._error;
  }

  set error(message: string) {
    this._error = message;
  }

  public getAllPrograms(): void {
    if (this._programs.length > 0 || this.loading) return;

    this._loading = true;
    const localStoredPrograms = StorageUtil.storageRead<Program[]>(
      StorageKeys.Programs
    );
    if (localStoredPrograms !== undefined) {
      this._programs = localStoredPrograms;
      this._loading = false;
      return;
    }

    this.http
      .get<Program[]>(`${backendAPI}${programsEndpoint}`)
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (data: Program[]) => {
          this._programs = [...data];
          StorageUtil.storageSave<Program[]>(
            StorageKeys.Programs,
            this._programs
          );
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
          console.log(error.message);
        },
      });
  }

  public addProgram(newProgram: NewProgram): Observable<string> {
    return this.http.post<string>(
      `${backendAPI}${programsEndpoint}`,
      newProgram,
      httpOptions
    );
  }

  public addWorkoutsToProgram(workoutIds: number[], programId: number) {
    return this.http.put(
      `${backendAPI}${programsEndpoint}/${programId}/workouts`,
      workoutIds,
      httpOptions
    );
  }
}
