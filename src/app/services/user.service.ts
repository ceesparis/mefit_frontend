import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';
import { StorageKeys } from '../enums/storage-keys.enum';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const {backendAPI, usersEndpoint , profileEndpoint, apiKey } = environment

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _user?: User;

  get user(): User | undefined {
    return this._user;
  }

  set user(user: User | undefined) {
    StorageUtil.storageSave<User>(StorageKeys.User, user!)
  this._user = user;
  }

  setProfileId(profileId: number){
    if (this._user != undefined){
      this._user.profileId = profileId;
      StorageUtil.storageSave<User>(StorageKeys.User, this._user)
      this.http.put(`${backendAPI}${usersEndpoint}/${this._user.id}/${profileEndpoint}`, profileId)
      .subscribe({
        error: (error: HttpErrorResponse) => {
          console.log(error.message)
        }
      })
    }
  }


  constructor(private readonly http: HttpClient) { 
    this._user = StorageUtil.storageRead<User>(StorageKeys.User);
  }

  // remove user from session storage
  public logOutUser(): void {
    StorageUtil.storageRemove(StorageKeys.User)
    StorageUtil.storageRemove(StorageKeys.Goal)
    this._user = undefined;
  }

}
