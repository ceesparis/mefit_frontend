import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, finalize, Observable, throwError, retry } from 'rxjs';
import { environment } from 'src/environments/environment';
import { FitnessLevel } from '../enums/fitness-level';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Goal } from '../models/goal.model';
import { Profile, ProfileResponse } from '../models/profile.model';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';
import { GoalService } from './goal.service';
import { UserService } from './user.service';

const { backendAPI, profilesEndpoint, apiKey } = environment;
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'x-api-key': apiKey,
  }),
};

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  // properties
  private _profile: Profile = {
    id: 0,
    firstName: '',
    lastName: '',
    birthday: new Date(),
    fitnessLevel: FitnessLevel.BEGINNER,
    heightCm: 0,
    weightKg: 0,
    pictureUrl: '',
  };
  private _loading: boolean = false;
  private _saving: boolean = false;
  private _error: string = '';

  // getters for properties
  get profile(): Profile {
    return this._profile;
  }
  get loading(): boolean {
    return this._loading;
  }
  get error(): string {
    return this._error;
  }
  get saving(): boolean {
    return this._saving;
  }
  // setters for properties - check if they are really necessary
  set profile(profile: Profile) {
    this._profile = profile;
  }
  set error(errorMessage: string) {
    this._error = errorMessage;
  }

  // constructor
  constructor(private readonly http: HttpClient,
     private userService: UserService,
     private goalService: GoalService,
    ) {}

  /*--------------------------------------------------------------
      Interaction with API
  --------------------------------------------------------------*/


  public createProfile(profile: Profile){
    this._loading = true;
    this.http.post<Profile>(`${backendAPI}${profilesEndpoint}`, profile)
    .pipe(
      finalize(() => {
        this._loading = false;
      })
    )
    .subscribe({
      next: (response) => {
        const resp: string = response.toString()
        const idString: string | undefined = resp.match(/(\d+)/)?.toString();
        if (idString != undefined){
          const id = parseInt(idString);
          if(this.userService.user != undefined){
            this.userService.setProfileId(id);
          }
        }
       
      }
    })
  }

  public getProfileById(profileId: number): void {
    this._loading = true;
    this.http
      .get<ProfileResponse>(`${backendAPI}${profilesEndpoint}/${profileId}`)
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (response: ProfileResponse) => {
          this._profile = {
            ...response,
          };
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
        },
      });
  }

  public getGoalsIdProfileById(profileId: number): void {
    var goals: number[] = []
    this._loading = true;
    this.http
      .get<ProfileResponse>(`${backendAPI}${profilesEndpoint}/${profileId}`)
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (response: ProfileResponse) => {
          console.log(response.goals)
          if(response.goals.length > 1){
            this.goalService.getGoalById(response.goals[response.goals.length-1])
          }
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
        },
      });
  }

  public updateProfile(profileToUpdate: Profile): Observable<Profile> {
    this._saving = true;

    return this.http
      .put<Profile>(
        `${backendAPI}${profilesEndpoint}/${profileToUpdate.id}`,
        profileToUpdate,
        httpOptions
      )
      .pipe(
        finalize(() => {
          this._saving = false;
        })
      )
      .pipe(
        retry(3), // retry a failed request up to 3 times
        catchError(this.handleError)
      );
  }

  /**
   * This method can be used as follows in a dedicated component:
   * this.profileService
    .addProfile(profileToAdd)
    .subscribe(profile => this.profiles.push(profile));
   */
  public addProfile(profileToAdd: Profile): Observable<Profile> {
    this._saving = true;
    return this.http
      .post<Profile>(
        `${backendAPI}${profilesEndpoint}`,
        profileToAdd,
        httpOptions
      )
      .pipe(
        finalize(() => {
          this._saving = false;
        })
      );
  }

  /*--------------------------------------------------------------
      Error Handler - based on Angular docs for HttpClient
  --------------------------------------------------------------*/

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `,
        error.error
      );
    }
    // Return an observable with a user-facing error message.
    return throwError(
      () => new Error('Something bad happened; please try again later.')
    );
  }
}