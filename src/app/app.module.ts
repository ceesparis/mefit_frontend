import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import {MatCardModule} from '@angular/material/card';
import {MatSliderModule} from '@angular/material/slider';
import {MatListModule} from '@angular/material/list';
import { ExerciseCataloguePage } from './pages/exercise-catalogue/exercise-catalogue.page';
import { ExerciseCatListItemComponent } from './components/exercise-catalogue/exercise-cat-list-item/exercise-cat-list-item.component';
import { ExerciseCatListComponent } from './components/exercise-catalogue/exercise-cat-list/exercise-cat-list.component';
import { WorkoutCatListComponent } from './components/workout-catalogue/workout-cat-list/workout-cat-list.component';
import { WorkoutCatListItemComponent } from './components/workout-catalogue/workout-cat-list-item/workout-cat-list-item.component';
import { CatalogueFilterComponent } from './components/catalogue-filter/catalogue-filter.component';
import { WorkoutCataloguePage } from './pages/workout-catalogue/workout-catalogue.page';
import { ProgramCataloguePage } from './pages/program-catalogue/program-catalogue.page';
import { ProgramCatListComponent } from './components/program-catalogue/program-cat-list/program-cat-list.component';
import { ProgramCatListItemComponent } from './components/program-catalogue/program-cat-list-item/program-cat-list-item.component';
import {MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { AppComponent } from './app.component';
import {MatToolbarModule} from '@angular/material/toolbar'; 
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner'; 
import {MatTabsModule} from '@angular/material/tabs';
import { MatIconModule} from '@angular/material/icon'; 
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatTooltipModule} from '@angular/material/tooltip'; 
import {MatGridListModule} from '@angular/material/grid-list';
import { FlexLayoutModule } from '@angular/flex-layout';

import { HttpAuthInterceptor } from './interceptors/auth-http.interceptor';
import { RefreshTokenHttpInterceptor } from './interceptors/refresh-token-http.interceptor';





import { LoginPage } from './pages/login/login.page';
import { ProfilePage } from './pages/profile/profile.page';
import { RegisterPage } from './pages/register/register.page';
import { ProgramEditComponent } from './components/program/program-edit/program-edit.component';
import { ContributionPage } from './pages/contribution/contribution.page';
import { WorkoutEditComponent } from './components/workout/workout-edit/workout-edit.component';
import { ExerciseEditComponent } from './components/exercise/exercise-edit/exercise-edit.component';
import { NavigationComponent } from './components/navigation/navigation.component';


import { DashboardPage } from './pages/dashboard/dashboard.page';
import { WorkoutSelectorComponent } from './components/dashboard/workout-selector/workout-selector.component';
import { ProgramSelectorComponent } from './components/dashboard/program-selector/program-selector.component';
import { CustomWorkoutCreatorComponent } from './components/dashboard/custom-workout-creator/custom-workout-creator.component';
import { WorkoutAddComponent } from './components/workout/workout-add/workout-add.component';
import { ProgramAddComponent } from './components/program/program-add/program-add.component';
import { ExerciseAddComponent } from './components/exercise/exercise-add/exercise-add.component';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ProfileInformationFormComponent } from './components/profile/profile-information-form/profile-information-form.component';
import { ProfileCredentialsFormComponent } from './components/profile/profile-credentials-form/profile-credentials-form.component';
import { ProfileAdvancedSettingsComponent } from './components/profile/profile-advanced-settings/profile-advanced-settings.component';
import { CataloguePage } from './pages/catalogue/catalogue.page';


@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    ExerciseCataloguePage,
    ExerciseCatListItemComponent,
    ExerciseCatListComponent,
    WorkoutCatListComponent,
    WorkoutCatListItemComponent,
    CatalogueFilterComponent,
    WorkoutCataloguePage,
    ProgramCataloguePage,
    ProgramCatListComponent,
    ProgramCatListItemComponent,
    ProfilePage,
    RegisterPage,
    ProgramEditComponent,
    ContributionPage,
    WorkoutEditComponent,
    ExerciseEditComponent,
    NavigationComponent,
    DashboardPage,
    WorkoutSelectorComponent,
    ProgramSelectorComponent,
    CustomWorkoutCreatorComponent,
    WorkoutAddComponent,
    ProgramAddComponent,
    ExerciseAddComponent,
    ProfileInformationFormComponent,
    ProfileCredentialsFormComponent,
    ProfileAdvancedSettingsComponent,
    CataloguePage,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatCardModule,
    MatDividerModule,
    ReactiveFormsModule,
    FormsModule,
    MatExpansionModule,
    DragDropModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatListModule,
    MatSliderModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatTooltipModule,
    MatGridListModule,
    FlexLayoutModule,
    HttpClientModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RefreshTokenHttpInterceptor,
      multi: true,
    },
    // Add HttpAuthInterceptor - Add Bearer Token to request
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpAuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
